EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "DIY ROTATING XY SURVEY CAMERA"
Date "2021-10-02"
Rev "0.0.1"
Comp "Open Source Hardware"
Comment1 "reuse & fork of project is authorized"
Comment2 "read LICENSE file for informations"
Comment3 "goblinrieur@gmail.com"
Comment4 "0.0.0 -> 0.0.1 little fix & logos on PCB"
$EndDescr
$Comp
L MCU_Microchip_PIC18:PIC18F452-IPT U301
U 1 1 61638FAF
P 4900 3650
F 0 "U301" H 5200 5000 50  0000 C CNN
F 1 "PIC18F452-IPT" H 5400 4900 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 4900 3650 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/39564c.pdf" H 4900 3650 50  0001 C CNN
	1    4900 3650
	1    0    0    -1  
$EndComp
Text HLabel 2050 1500 2    50   Input ~ 0
VCC
$Comp
L Device:R_US R301
U 1 1 6163FD55
P 2000 1700
F 0 "R301" V 1795 1700 50  0000 C CNN
F 1 "10k" V 1886 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2040 1690 50  0001 C CNN
F 3 "~" H 2000 1700 50  0001 C CNN
	1    2000 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C301
U 1 1 61640A4F
P 2000 2100
F 0 "C301" H 2115 2146 50  0000 L CNN
F 1 "100nf" H 2115 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2038 1950 50  0001 C CNN
F 3 "~" H 2000 2100 50  0001 C CNN
	1    2000 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 6164207B
P 2000 2300
F 0 "#PWR0110" H 2000 2050 50  0001 C CNN
F 1 "GND" H 2005 2127 50  0000 C CNN
F 2 "" H 2000 2300 50  0001 C CNN
F 3 "" H 2000 2300 50  0001 C CNN
	1    2000 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1500 2000 1500
Wire Wire Line
	2000 1500 2000 1550
Wire Wire Line
	2000 2250 2000 2300
Wire Wire Line
	2000 1850 2000 1900
Wire Wire Line
	3900 2650 2800 2650
Wire Wire Line
	2800 2650 2800 1900
Wire Wire Line
	2800 1900 2000 1900
Connection ~ 2000 1900
Wire Wire Line
	2000 1900 2000 1950
NoConn ~ 3900 2850
NoConn ~ 3900 2950
NoConn ~ 3900 3050
NoConn ~ 3900 3150
NoConn ~ 3900 3250
NoConn ~ 3900 3350
NoConn ~ 5900 4450
NoConn ~ 5900 4550
NoConn ~ 5900 4650
$Comp
L Device:Crystal Y301
U 1 1 61647896
P 2650 3450
F 0 "Y301" V 2604 3581 50  0000 L CNN
F 1 "10Mhz" V 2695 3581 50  0000 L CNN
F 2 "Crystal:Crystal_HC18-U_Horizontal" H 2650 3450 50  0001 C CNN
F 3 "~" H 2650 3450 50  0001 C CNN
	1    2650 3450
	0    1    1    0   
$EndComp
$Comp
L Device:C C302
U 1 1 616488C3
P 2300 3250
F 0 "C302" V 2048 3250 50  0000 C CNN
F 1 "22p" V 2139 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2338 3100 50  0001 C CNN
F 3 "~" H 2300 3250 50  0001 C CNN
	1    2300 3250
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 61648FAB
P 2000 3900
F 0 "#PWR0111" H 2000 3650 50  0001 C CNN
F 1 "GND" H 2005 3727 50  0000 C CNN
F 2 "" H 2000 3900 50  0001 C CNN
F 3 "" H 2000 3900 50  0001 C CNN
	1    2000 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3250 2000 3250
Wire Wire Line
	2000 3250 2000 3650
Connection ~ 2000 3650
Wire Wire Line
	2000 3650 2000 3900
Wire Wire Line
	2450 3250 2650 3250
Wire Wire Line
	2650 3250 2650 3300
Wire Wire Line
	2650 3600 2650 3650
Wire Wire Line
	2650 3650 2450 3650
Wire Wire Line
	2150 3650 2000 3650
$Comp
L Device:C C303
U 1 1 61648DA3
P 2300 3650
F 0 "C303" V 2048 3650 50  0000 C CNN
F 1 "22pf" V 2139 3650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2338 3500 50  0001 C CNN
F 3 "~" H 2300 3650 50  0001 C CNN
	1    2300 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 3450 3150 3450
$Comp
L power:GND #PWR0112
U 1 1 6164B545
P 4850 5150
F 0 "#PWR0112" H 4850 4900 50  0001 C CNN
F 1 "GND" H 4855 4977 50  0000 C CNN
F 2 "" H 4850 5150 50  0001 C CNN
F 3 "" H 4850 5150 50  0001 C CNN
	1    4850 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4950 4800 5050
Wire Wire Line
	4800 5050 4850 5050
Wire Wire Line
	4850 5050 4850 5150
Connection ~ 4850 5050
Wire Wire Line
	4850 5050 4900 5050
Text Label 1950 1500 2    50   ~ 0
+5v
Text Label 4850 2200 2    50   ~ 0
+5v
Wire Wire Line
	2000 1500 1950 1500
Connection ~ 2000 1500
Wire Wire Line
	4900 2350 4900 2300
Wire Wire Line
	4800 2300 4800 2350
Wire Wire Line
	4850 2300 4850 2200
Wire Wire Line
	4850 2300 4800 2300
$Comp
L Connector:Conn_01x03_Female J301
U 1 1 6164DD72
P 2100 5800
F 0 "J301" V 1946 5948 50  0000 L CNN
F 1 "Servo0" V 2037 5948 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B3B-EH-A_1x03_P2.50mm_Vertical" H 2100 5800 50  0001 C CNN
F 3 "~" H 2100 5800 50  0001 C CNN
	1    2100 5800
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Female J302
U 1 1 6164EA4D
P 2900 5800
F 0 "J302" V 2746 5948 50  0000 L CNN
F 1 "Servo1" V 2837 5948 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B3B-EH-A_1x03_P2.50mm_Vertical" H 2900 5800 50  0001 C CNN
F 3 "~" H 2900 5800 50  0001 C CNN
	1    2900 5800
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R303
U 1 1 6164F682
P 2800 5000
F 0 "R303" H 2868 5046 50  0000 L CNN
F 1 "1k" H 2868 4955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2840 4990 50  0001 C CNN
F 3 "~" H 2800 5000 50  0001 C CNN
	1    2800 5000
	1    0    0    -1  
$EndComp
Text HLabel 2350 5200 1    50   Output ~ 0
5v_servos
Text Label 2400 5350 2    50   ~ 0
5vs
Text Label 3200 5350 2    50   ~ 0
5vs
Wire Wire Line
	3200 5350 2900 5350
Wire Wire Line
	2800 5600 2800 5150
Wire Wire Line
	2400 5350 2100 5350
NoConn ~ 3900 4150
NoConn ~ 3900 4250
NoConn ~ 3900 4350
NoConn ~ 3900 4450
NoConn ~ 3900 4550
Wire Wire Line
	2000 5600 2000 5150
$Comp
L Device:R_US R302
U 1 1 6164FE63
P 2000 5000
F 0 "R302" H 1800 5050 50  0000 L CNN
F 1 "1k" H 1800 4950 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2040 4990 50  0001 C CNN
F 3 "~" H 2000 5000 50  0001 C CNN
	1    2000 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 5350 2900 5600
Wire Wire Line
	2100 5350 2100 5600
$Comp
L power:GND #PWR0113
U 1 1 6166811B
P 2600 5800
F 0 "#PWR0113" H 2600 5550 50  0001 C CNN
F 1 "GND" H 2605 5627 50  0000 C CNN
F 2 "" H 2600 5800 50  0001 C CNN
F 3 "" H 2600 5800 50  0001 C CNN
	1    2600 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 61668397
P 3400 5800
F 0 "#PWR0114" H 3400 5550 50  0001 C CNN
F 1 "GND" H 3405 5627 50  0000 C CNN
F 2 "" H 3400 5800 50  0001 C CNN
F 3 "" H 3400 5800 50  0001 C CNN
	1    3400 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5600 3000 5500
Wire Wire Line
	3000 5500 3400 5500
Wire Wire Line
	3400 5500 3400 5800
Wire Wire Line
	2600 5800 2600 5500
Wire Wire Line
	2600 5500 2200 5500
Wire Wire Line
	2200 5500 2200 5600
Text Notes 1500 6800 0    50   ~ 0
WARNING : depending on your servo model + signal and gnd are connected differently\nmultiplex +-signal\nrobbe/futaba/graupner -+signal\netc...\n
Wire Wire Line
	2800 3950 2800 4850
Text GLabel 3550 4050 0    50   Input ~ 0
RB2
Wire Wire Line
	3900 4050 3550 4050
Text GLabel 7550 2500 0    50   Input ~ 0
RB2
$Comp
L Device:R_US R304
U 1 1 61679A1A
P 7950 2500
F 0 "R304" V 7745 2500 50  0000 C CNN
F 1 "1k" V 7836 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7990 2490 50  0001 C CNN
F 3 "~" H 7950 2500 50  0001 C CNN
	1    7950 2500
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_EBC Q301
U 1 1 6167AEDD
P 8450 2500
F 0 "Q301" H 8640 2546 50  0000 L CNN
F 1 "2222" H 8640 2455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8650 2600 50  0001 C CNN
F 3 "~" H 8450 2500 50  0001 C CNN
	1    8450 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 6167B67B
P 8550 2800
F 0 "#PWR0115" H 8550 2550 50  0001 C CNN
F 1 "GND" H 8555 2627 50  0000 C CNN
F 2 "" H 8550 2800 50  0001 C CNN
F 3 "" H 8550 2800 50  0001 C CNN
	1    8550 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 2700 8550 2800
Wire Wire Line
	8250 2500 8100 2500
Wire Wire Line
	7800 2500 7550 2500
Text HLabel 9050 2000 2    50   BiDi ~ 0
gndcam
Wire Wire Line
	9050 2000 8550 2000
Wire Wire Line
	8550 2000 8550 2300
Wire Wire Line
	2350 5200 2350 5250
Wire Wire Line
	2350 5250 2100 5250
Wire Wire Line
	2100 5250 2100 5350
Connection ~ 2100 5350
Wire Wire Line
	4900 4950 4900 5050
$Comp
L Device:R_US R502
U 1 1 616972EB
P 7400 3250
F 0 "R502" V 7195 3250 50  0000 C CNN
F 1 "10k" V 7286 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7440 3240 50  0001 C CNN
F 3 "~" H 7400 3250 50  0001 C CNN
	1    7400 3250
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R501
U 1 1 616990F7
P 7900 3350
F 0 "R501" V 8000 3350 50  0000 C CNN
F 1 "10k" V 8100 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7940 3340 50  0001 C CNN
F 3 "~" H 7900 3350 50  0001 C CNN
	1    7900 3350
	0    1    1    0   
$EndComp
Text HLabel 9050 3250 2    50   Output ~ 0
tx_cam
Text HLabel 9050 3350 2    50   Output ~ 0
rx_cam
Text HLabel 6550 3550 2    50   BiDi ~ 0
RD0
Text HLabel 6550 3650 2    50   BiDi ~ 0
RD1
Text HLabel 6550 3750 2    50   BiDi ~ 0
RD2
Text HLabel 6550 3850 2    50   BiDi ~ 0
RD3
Text HLabel 6550 3950 2    50   BiDi ~ 0
RD4
Text HLabel 6550 4050 2    50   BiDi ~ 0
RD5
Text HLabel 6550 4150 2    50   BiDi ~ 0
RD6
Text HLabel 6550 4250 2    50   BiDi ~ 0
RD7
Wire Wire Line
	6550 3550 5900 3550
Wire Wire Line
	5900 3650 6550 3650
Wire Wire Line
	6550 3750 5900 3750
Wire Wire Line
	5900 3850 6550 3850
Wire Wire Line
	6550 3950 5900 3950
Wire Wire Line
	5900 4050 6550 4050
Wire Wire Line
	6550 4150 5900 4150
Wire Wire Line
	5900 4250 6550 4250
Text HLabel 6550 2650 2    50   Output ~ 0
RC0
Text HLabel 6550 2750 2    50   Output ~ 0
RC1
Text HLabel 6550 2850 2    50   Output ~ 0
RC2
Text HLabel 6550 2950 2    50   Output ~ 0
RC3
Text HLabel 6550 3050 2    50   Output ~ 0
RC4
Text HLabel 6550 3150 2    50   Output ~ 0
RC5
Wire Wire Line
	6550 2650 5900 2650
Wire Wire Line
	5900 2750 6550 2750
Wire Wire Line
	6550 2850 5900 2850
Wire Wire Line
	5900 2950 6550 2950
Wire Wire Line
	6550 3050 5900 3050
Wire Wire Line
	5900 3150 6550 3150
Wire Wire Line
	9050 3250 7550 3250
Wire Wire Line
	7250 3250 5900 3250
Wire Wire Line
	5900 3350 7750 3350
Wire Wire Line
	9050 3350 8050 3350
Wire Wire Line
	4850 2300 4900 2300
Connection ~ 4850 2300
Wire Wire Line
	2650 3650 3900 3650
Connection ~ 2650 3650
Wire Wire Line
	3150 3250 2650 3250
Wire Wire Line
	3150 3250 3150 3450
Connection ~ 2650 3250
Text Notes 7000 4250 0    50   ~ 0
RD BUS\nEthernet \nmodule
Wire Wire Line
	3900 3950 2800 3950
Wire Wire Line
	2500 3850 2500 4400
Wire Wire Line
	2500 4400 2000 4400
Wire Wire Line
	2000 4400 2000 4850
Wire Wire Line
	2500 3850 3900 3850
$EndSCHEMATC
