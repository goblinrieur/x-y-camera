# X-Y camera

Minimalist survey camera with Ethernet module TCP/IP & 2 servo-motors.

Main Idea is to be easy to redesign with your own materials you have in your own stock.

Project is using a very permissive license to be shared to any one, do not hesitate to copy / fork make your own version & so on.

Read ![This is free and unencumbered software released into the public domain.](./LICENSE) for details on your permissions.

# How it is build around 

-	Power supply _of course_

-	WWW -> wiz810 module _(can here again be replaced by any similar function module)_ 

-	Pic 18F452 _(because I 've got it in stock, can be adapted to many µC)_

-	2 servo-motors _(standard)_  for _(elevation & rotation)_

-	uCam-232 _(can be adapted to many diy cameras designed for raspberry or arduino & so on)_

# Bloc diagram & equipment

![concept](./pics/blocdiagram.png)

# Good practice for easier work

Make your schematic around the bloc diagram like I do.

![schema pages in pdf](./schema.pdf)

Connect each main separated functions with wires, & inside each functions use the kicad schematic tools.

# In the example I gave this work will result on quite small PCB

![1](./1.png)

![2](./2.png)

![3](./3.png)

![4](./4.png)

![5](./5.png)

![6](./6.png)

notice for components that can heat up, I use vias to the ground plane to limit that.

do not forget to add holes for fixation in a box !!!

# How does it work for mechanical part (servomotors)

- turning its own left  : use 2ms duty cycle from a 20ms period until signal stops or maximum angle reached. 

- turning its own right : use 1ms duty cycle from a 20ms period until signal stops or maximum angle reached. 

- do not rotate 	: use 1.5ms duty cycle still on 20ms period until duty cycle changes

You have to design that on your code.

# Code parts

Your code must use only libraries and commands authorized for the micro controller you re using. Be careful also to adapt code if you change camera or Ethernet boards.

It might be something like :

- main.c 	main code of the project

- camera.c 	to code camera relative parts _(capture of pictures & acknowledgement at least)_

- eeprom.c	to manage eeprom of the PIC micro controller _(in this example it is a pic, but I repeat, you can adapt to your own needs)_

- HTML.c	to manage character strings as HTML _(might not be the better method but it is quite simple & solid enough for this project)_

- interrupt.c	to manage interruption on timers

- wiz810.c	to manage internet/Ethernet module & its TCP/IP connections

# Your web server 

Your web server must be able to send orders to the project board 

HTTP request should look like

`http://localhost.home/index.html` on the computer/phone/anything

Interpreted by micro controller would be like 

`GET /index.html HTTP/1.1 (some orders)`

example :

`http://localhost.home/pix/jpg ? x=10 & y=55 & 432345654` will be interpreted as `GET /pix.jpg ? x=10 & y=55 & 432345654`

Your code from that information has now only to drive servomotors to get corresponding X/Y 10/55 rotations, 432345654 is a HTTP request ID it changes each time & guaranties the picture on the web browser is the last one.

So of course It is working with an acknowledgement, before capture of camera image.


```
HTTP/1.1 200 OK
Connection: close
Context-Type: text/html
Content-Length: 4500
```

Taking picture is working the same way with other HTTP commands of GET type.

As you see all is inter-dependant : code/web server/hardware & so on so be patient _you may make errors & debug before it works_ don't worry it is normal.

You can build your own web server or use Open-Source ones, if you do not want to do this part by yourself of course.

I don't give an example cause I use other people Open-Source ones myself :) 

# code parts 

As reflection example here are some parts of code for the project you can get inspired of :

Here is what a move & get picture sub-routine can look like _(in C language)_ for main.c 

```
  [...]
  // in a switch case structure for each kind of request & actions
  do {
  //------ Test if 'PIX.JPG' + move SERVO ----------
  // from HTTP request  variables x & y 
  x = Strinstr("pix.jpg?x=");
  if (x!=0xff) {
    // rotate servo X
    if (nopass) {
      // get % X position
      ddx = Strtoint(x+10);
      if (ddx>100) ddx=100;
      ddx = 100-ddx;
      ddx *= (POSMAX-POSMIN);
      ddx = (ddx/100)+POSMIN;

      // does the same for servo Y
      x = Strinstr("&y=");
      ddy = Strtoint(x+3);
      if (ddy>100) ddy=100;
      ddy *= (POSMAX-POSMIN);
      ddy = (ddy/100)+POSMIN;
      
      // rotate it now
      Servo((unsigned char)ddx,(unsigned char)ddy);
      // then capture image
      // and send it back to web browser
      GoCapture(resol);
    } else
    break;
  }
  [...]
```

See above we use a function named Servo(x,y) to move them to the desired position, this function might look like 

```
void Servo(unsigned char px,unsigned char py) {

 XPOS = px;
 YPOS = py;
 
 if (XPOS<POSMIN) XPOS=POSMIN;
 if (XPOS>POSMAX) XPOS=POSMAX;

 if (YPOS<POSMIN) YPOS=POSMIN;
 if (YPOS>POSMAX) YPOS=POSMAX;
 
 INTCON = VALID_IT;		// interruption call Gie=1, PEIE=1, TMR0IE=1
 TempoMS(250);			// tempo ms
 TempoMS(250);			// tempo ms
 INTCON = 0;			// stop interruption
}
```

# Now 

It's all to you now, to choose your own Ethernet module, micro controller _(atmel,pic,esp32,raspberry2040,etc..)_ or development board like _(arduino,raspberry pico, picaxe, etc...)_, your camera of course & your device to run a web application or why not a GUI from your smartphone wrote in python using public HTTP relay to communicate with your own project board.

Have fun.
